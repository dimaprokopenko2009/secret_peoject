Function global:Prokop_Get-AddUser{
    <#
    .Synopsis
        Add an user in secret database.
    .Description
        Add an user different types in secret database.
        This function appends and saves three user:
        - Credential;
        - Token;
        - SSH.
    .Parameter Description
        This parameter takes the types of user as a variable.
    .Example
        Prokop_Get-AddUser Credential

        #=============================================
        Enter full name, place: Dmitry
        Enter login: pro
        Enter secret value: 123
        Enter URL: www.lol.com
        Enter expires time in format YYYYMMDD: 20191001
        Enter tag: first tag
        #=============================================
        [
            {
                "tag":  [
                            "first tag"
                        ],
                "user":  "Dmitry",
                "expiresTime":  "20191001",
                "secret_value":  "123",
                "URL":  "www.lol.com",
                "log":  "pro",
                "history":  [
                            "123"
                            ]

            }
        ]
        #=============================================
    #>  
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)] 
        [String] $User,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        New-Item -Path $Path -ItemType "file" | Out-Null
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    }
    #=========================================================================================
    Function Common ()
    {   
        Param([string]$arg1, [string]$arg2)
        $user = Read-Host "Enter full name, place"
        $URL = Read-Host "Enter URL"
        $expiresTime = Read-Host "Enter expires time in format YYYYMMDD"
        $newData = @{
                    user = $user
                    $arg1 = $log
                    $arg2 = $passwd
                    URL = $URL
                    tag = @()
                    expiresTime = $expiresTime
                    history = @()
        }

        $baseObject = , $newData + $baseObject
        $baseObject | ConvertTo-Json | Set-Content -Path $Path 
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json

        $tags = Read-Host "Enter tag"
        $baseObject[0].tag += $tags
        $baseObject[0].history +=$passwd
        $baseObject | ConvertTo-Json | Set-Content -Path $Path 
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    }
    #=========================================================================================
    if ($User -ceq "Credential") {
        $log = Read-Host "Enter login"
        $passwd = Read-Host "Enter passwd" #-AsSecureString
        Common log passwd
    } elseif ($User -ceq "Token"){
        $log = Read-Host "Enter login"
        $passwd = Read-Host "Enter secret value" #-AsSecureString
        Common log secret_value
    } elseif ($User -ceq "SSH"){
        $log = Read-Host "Enter name"
        $passwd = Read-Host "Enter value of key" #-AsSecureString
        Common name value_of_key
    }
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Get-UserData{
    <#
    .Synopsis
        Output credentials with secret database on display.
    .Description
        The function returns detailed credeintials, in secret database,
        about user, that request.
    .Parameter Secret
        This parameter takes the username as a variable.
    .Example
        Prokop_Get-UserData Dmitry

        #=============================================
        tag          : {first tag}
        user         : Dmitry
        expiresTime  : 20191001
        secret_value : {123}
        URL          : www.lol.com
        log          : pro
        #=============================================
    #>  
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)] 
        [String] $Secret,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================    
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    $supplist = @()
    ForEach ($u in $baseObject){ 
        if ($u.user -ceq $Secret) {
            if ($u.log -and $u.passwd) {
                $lol = @{
                    user = $u.user
                    log = $u.log
                    passwd = $u.passwd
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }elseif ($u.log -and $u.secret_value) {
                $lol = @{
                    user = $u.user
                    log = $u.log
                    secret_value = $u.secret_value
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }elseif ($u.name -and $u.value_of_key) {
                $lol = @{
                    user = $u.user
                    name = $u.name
                    value_of_key = $u.value_of_key
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }
            $supplist += , $lol
        }
    }
    $list = @($supplist | ConvertTo-Json | ConvertFrom-Json)
    return $list
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Password-Generated{
    <#
    .Synopsis
        Generate password for user.
    .Description
        The function generate password type in the format:
        20DAa%A3D3a3;
        #(20) - length password;
        Типы:
        D – any digit;
        a – any small letter in latin alphabet;
        A – any uppercase letter in latin alphabet;
        % – format line;
        digit – number of characters of the specified type.
        Autput result on display.
    .Parameter Set
        This parameter contains the generate password format.
    .Example
        Prokop_Password-Generated 20DAa%A3D3a3

        #=============================================
        JsBTKLlz8A0H3E6wWKg4
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [String] $Set        
    )
    $A = "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M","N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    $D = "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
    #=========================================================================================
    Function Count (){
    Param($arg1, [int]$arg2)
    for($i=1; $i -le $arg2; $i++){
        $k = ($arg1 | Get-Random) | Get-Random
        $count += $k
        }
    return $count
    }
    #=========================================================================================
    if ($Set -cmatch "%") {
        $Set -cmatch "^(?<first>[0-9]+)(?<second>[DAa]+)%(?<third>[AaD0-9]+)" | Out-Null
        $first = $Matches.first
        $second = $Matches.second
        $third = $Matches.third

        $groupsThird = ([regex]::matches($third, "[a-zA-Z]{1}[0-9]+")).Value

        $groupsThird | ForEach-Object { 
            $groupsINT1 = ([regex]::matches($_, "[0-9]+")).Value 
            $intThird += [int]$groupsINT1
            $groupsINT2 = ([regex]::matches($_, "[a-zA-Z]{1}$")).Value 
            $intThird += $groupsINT2.Length}
        
        $lists = @()
        if ( $second -cmatch "A" ) {
            $lists += $A }
        if ( $second -cmatch "a" ) {
            $lists += $A.ToLower() }
        if ( $second -cmatch "D" ) {
            $lists += $D }
            
        $count += Count ($lists) ([int]$first - $intThird)

        $groupsThird | ForEach-Object { 
            $letter = ([regex]::matches($_, "[a-zA-Z]{1}").Value)
            $quantity = ([regex]::matches($_, "[0-9]+").Value)
            if ( $letter -ceq "a") {
                $count += Count @($A.ToLower()) ([int]$quantity)
            } elseif ( $letter -ceq "A") {
                $count += Count @($A) ([int]$quantity)
            } elseif ( $letter -ceq "D") {
                $count += Count @($D) ([int]$quantity)
            };   
        }
    #=========================================================================================
    } else {
        $Set -cmatch "^(?<first>[0-9]+)(?<second>[DAa]+)" | Out-Null
        $first = $Matches.first
        $second = $Matches.second
        
        $lists = @()
        if ( $second -cmatch "A" ) {
            $lists += $A }
        if ( $second -cmatch "a" ) {
            $lists += $A.ToLower() }
        if ( $second -cmatch "D" ) {
            $lists += $D }
        
        $count += Count ($lists) ([int]$first)
    }
    #=========================================================================================
    $rang = 0..($count.Length - 1) | Get-Random -Count ([int]::MaxValue)
    $password += [string]$count[$rang]
    $password = $password -replace '\s',''
    return $password
}
#============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Get-DiapasonExpiresTime{
    <#
    .Synopsis
        Output on display "expires time" through ends in K days.
    .Description
        The function returns credentials, which ends in K days.
    .Parameter Days
        This parameter takes the number of days as a variable.
    .Example
        Prokop_Get-DiapasonExpiresTime 37

        #=============================================
        tag          : {first tag}
        user         : Dmitry
        expiresTime  : 20191001
        secret_value : {123}
        URL          : www.lol.com
        log          : pro
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)] 
        [int] $Days,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    $supplist = @()
    $TimeSpan = (Get-Date) - (Get-Date).AddDays(-$Days)
    $date = (Get-Date) + $TimeSpan
    ForEach ($u in $baseObject){ 
        $p = [datetime]::ParseExact($u.expiresTime,'yyyyMMdd',$null) -replace '\s',''
        $d = $date.Date -replace '\s',''
        if ($p -ceq $d) {
            if ($u.log -and $u.passwd) {
                $lol = @{
                    user = $u.user
                    log = $u.log
                    passwd = $u.passwd
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }elseif ($u.log -and $u.secret_value) {
                $lol = @{
                    user = $u.user
                    log = $u.log
                    secret_value = $u.secret_value
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }elseif ($u.name -and $u.value_of_key) {
                $lol = @{
                    user = $u.user
                    name = $u.name
                    value_of_key = $u.value_of_key
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }

            $supplist += , $lol
        }
    }
    $list = @($supplist | ConvertTo-Json | ConvertFrom-Json)
    return $list
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Extension-ExpiresTime{
    <#
    .Synopsis
        Extension "expires time" in K days.
    .Description
        The function makes the extension "expire time" in K days
        for username entered.
    .Parameter Days
        This parameter takes the number of days as a variable.
    .Parameter Name
        This parameter takes the username as a variable.
    .Example
        Prokop_Extension-ExpiresTime Dmitry 10

        #=============================================
        [
            {
                "tag":  [
                            "first tag"
                        ],
                "user":  "Dmitry",
                "expiresTime":  "20191011",
                "secret_value":  "123",
                "URL":  "www.lol.com",
                "log":  "pro",
                "history":  [
                            "123"
                            ]
            }
        ]
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)] 
        [string] $Name,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [int] $Days,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    $TimeSpan = (Get-Date) - (Get-Date).AddDays(-$Days)
    ForEach ($u in $baseObject){ 
        #if ([string]$u.user -ceq $Name ) {
        if ($u.user -ceq $Name ) {
            $dateNow = [datetime]::ParseExact($u.expiresTime,'yyyyMMdd',$null)
            $date = $dateNow + $TimeSpan

            $date -cmatch "^(?<first>[0-9]+)/(?<second>[0-9]+)/(?<third>[0-9]+)" | Out-Null
            $first = $Matches.first
            $second = $Matches.second
            $third = $Matches.third

            $u.expiresTime = $third+$first+$second
            $baseObject | ConvertTo-Json | Set-Content -Path $Path
        }
    }
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Update-ExpiresTime{
    <#
    .Synopsis
        Changing the value of the field "expires time" for user.
    .Description
        The function changes the value "time is running out" 
        to the specified date for the entered user name.
        Format date:
        YYYYMMDD
    .Parameter Date
        This parameter takes the specified date as a variable.
    .Parameter Name
        This parameter takes the username as a variable.
    .Example
        Prokop_Update-ExpiresTime Dmitry 20201231

        #=============================================
        [
            {
                "tag":  [
                            "first tag"
                        ],
                "user":  "Dmitry",
                "expiresTime":  "20201231",
                "secret_value":  "123",
                "URL":  "www.lol.com",
                "log":  "pro",
                "history":  [
                            "123"
                            ]
            }
        ]
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)] 
        [string] $Name,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [string] $Date,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    ForEach ($u in $baseObject){ 
        if ([string]$u.user -ceq $Name ) {
            $u.expiresTime = $Date
            $baseObject | ConvertTo-Json | Set-Content -Path $Path
        }
    }
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Del-Secret{
    <#
    .Synopsis
        Delete secret by field "name".
    .Description
        The function delete secret by field "name" in secret database.
    .Parameter Name
        This parameter takes the username as a variable.
    .Example
        Prokop_Del-Secret Dmitry

        #=============================================
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Name,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    $newBaseObject = @()
    ForEach ($u in $baseObject){ 
        if ($u.user -cne $Name ) {
            $newBaseObject += $u
        }
        $newBaseObject | ConvertTo-Json | Set-Content -Path $Path
    }
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Update-Secret{
    <#
    .Synopsis
        Changes to secret field values for user.
    .Description
        The function сhanges to secret field values for user in secret database.
    .Parameter Name
        This parameter takes the username as a variable.
    .Parameter key
        This parameter takes the name key as a variable.
    .Parameter value
        This parameter takes the value key as a variable.
    .Example
        Prokop_Update-Secret Dmitry log prokop

        #=============================================
        [
            {
                "tag":  [
                            "first tag"
                        ],
                "user":  "Dmitry",
                "expiresTime":  "20201231",
                "secret_value":  "123",
                "URL":  "www.lol.com",
                "log":  "prokop",
                "history":  [
                            "123"
                            ]
            }
        ]
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Name,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $key,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $value,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    ForEach ($u in $baseObject){ 
        if ($u.user -ceq $Name ) {
            $u.$key = $value
            if ($key -ceq "passwd"){
                $u.history += , $value
            }elseif ($key -ceq "secret_alue"){
                $u.history += , $value
            }elseif ($key -ceq "value_of_key"){
                $u.history += , $value
            }
            $baseObject | ConvertTo-Json | Set-Content -Path $Path
        }
    }
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Get-ListUser{
    <#
    .Synopsis
        Get a list of secrets by field "name".
    .Description
        The function get a list of secrets by field "name" in secret database.
    .Parameter Name
        This parameter takes the username as a variable.
    .Example
        Prokop_Get-ListUser Dmitry

        #=============================================
        tag          : {first tag}
        user         : Dmitry
        expiresTime  : 20201231
        secret_value : {123}
        URL          : www.lol.com
        log          : prokop
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Name,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    $supplist = @()
    ForEach ($u in $baseObject){ 
        if ($u.user -ceq $Name ) {
            if ($u.log -and $u.passwd) {
                $lol = @{
                    user = $u.user
                    log = $u.log
                    passwd = $u.passwd
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }elseif ($u.log -and $u.secret_value) {
                $lol = @{
                    user = $u.user
                    log = $u.log
                    secret_value = $u.secret_value
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }elseif ($u.name -and $u.value_of_key) {
                $lol = @{
                    user = $u.user
                    name = $u.name
                    value_of_key = $u.value_of_key
                    URL = $u.URL
                    tag = $u.tag
                    expiresTime = $u.expiresTime
                }
            }
            $supplist += , $lol
        }
    }
    $list = @($supplist | ConvertTo-Json | ConvertFrom-Json)
    return $list
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Update-Tags{
    <#
    .Synopsis
        Change tags by field "name".
    .Description
        The function change tags by field "name" in secret database.
    .Parameter Name
        This parameter takes the username as a variable.
    .Parameter tags
        This parameter takes the new tag as a variable.
    .Example
        Prokop_Update-Tags Dmitry second

        #=============================================
        [
            {
                "tag":  [
                            "second tag"
                        ],
                "user":  "Dmitry",
                "expiresTime":  "20201231",
                "secret_value":  "System.Security.SecureString",
                "URL":  "www.lol.com",
                "log":  "prokop",
                "history":  [
                            "123"
                            ]
            }
        ]
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Name,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $tags,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    $newTag = @()
    ForEach ($u in $baseObject){ 
        if ($u.user -ceq $Name ) {
            $newTag += $tags
            $u.tag = $newTag
            $baseObject | ConvertTo-Json | Set-Content -Path $Path
        }
    }
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_Add-Tags{
    <#
    .Synopsis
        Add tag by field "name".
    .Description
        The function add tag by field "name" in secret database.
    .Parameter Name
        This parameter takes the username as a variable.
    .Parameter tags
        This parameter takes the new tag as a variable.
    .Example
        Prokop_Add-Tags Dmitry "new tag"

        #=============================================
        [
            {
                "tag":  [
                            "second tag",
                            "new tag"
                        ],
                "user":  "Dmitry",
                "expiresTime":  "20201231",
                "secret_value":  "123",
                "URL":  "www.lol.com",
                "log":  "prokop",
                "history":  [
                            "123"
                            ]
            }
        ]
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Name,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $tag,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    ForEach ($u in $baseObject){ 
        if ($u.user -ceq $Name ) {
            $u.tag += $tag
            $baseObject | ConvertTo-Json | Set-Content -Path $Path
        }
    }
    #=========================================================================================
}
#============================================================================================================================================        #============================================================================================================================================
#============================================================================================================================================
Function global:Prokop_History-Password{
    <#
    .Synopsis
        View history passwords for user.
    .Description
        The function shows history passwords for user in secret database.
    .Parameter Name
        This parameter takes the username as a variable.
    .Example
        Prokop_History-Password Dmitry
        #=============================================
        123
        321
        #=============================================
    #> 
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Name,
        [String] $Path = "D:\PoweShell\secret.json"
    )
    #=========================================================================================
    $isfile = Test-Path $Path 
    if($isfile) {
        $baseObject = Get-Content -Path $Path | ConvertFrom-Json
    } else {
        Write-Host "File not found."
    }
    #=========================================================================================
    ForEach ($u in $baseObject){ 
        if ($u.user -ceq $Name ) {
            $u.history
        }
    }
    #=========================================================================================
}